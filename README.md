Hewwos!

I bapped at the keyboard and now my Internet looks different.

This is my collection of userstyles! To make your Internet look different also, you need a userstyle plugin for your web browser, like [Stylus](https://add0n.com/stylus.html).

Feel free to boop me or [leave a comment](/LexxyFox/userstyles/issues/new) if you has anything you'd like to say :3

Also, go check out [Freeplay's pawesome styles](https://codeberg.org/Freeplay/UserStyles).

[![DON'T upload to GitHub](https://no-github.bitbucket.io/badge.svg)](https://no-github.bitbucket.io)

## Styles

* [duckduckgo.com](https://lexxyfox.codeberg.page/userstyles/duckduckgo.user.css)
* [CAPTCFA](https://lexxyfox.codeberg.page/userstyles/captcfa.user.css)
